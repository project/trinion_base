<?php
require_once __DIR__ . '/../../../../autoload.php';
require_once __DIR__ . '/../src/WSServerApplications/WServer.php';
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$server = new \Bloatless\WebSocket\Server('127.0.0.1', 3000, './phpwss.sock');

// server settings:
$server->setMaxClients(500);
$server->setCheckOrigin(false);
$server->setAllowedOrigin('www.trinion.org');
$server->setMaxConnectionsPerIp(500);

$server->registerApplication('ws', \Drupal\trinion_base\WSServerApplication\WServer::getInstance());

$server->run();
