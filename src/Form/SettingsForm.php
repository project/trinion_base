<?php

namespace Drupal\trinion_base\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;


class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trinion_base_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['trinion_base.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['link'] = [
      '#theme'=> 'item_list',
      '#title' => 'That module use',
      '#html' => TRUE,
      '#items' => [
        ['#markup' => '<a href="/admin/trinion_users">' . t('Users list') . '</a>'],
      ],
    ];
    $form['caption'] = [
      '#markup' => '<h3>' . t('Fill this fields') . '</h3>',
    ];
    $form['backend_theme_name'] = [
      '#type' => 'textfield',
      '#title' => t('Theme name'),
      '#required' => TRUE,
      '#default_value' => $this->config('trinion_base.settings')->get('backend_theme_name'),
    ];
    $form['host'] = [
      '#type' => 'textfield',
      '#title' => t('Current site host'),
      '#description' => t('With no trailing slash. Example: http://example.com'),
      '#required' => TRUE,
      '#default_value' => $this->config('trinion_base.settings')->get('host'),
    ];
    $form['title_login_and_password_page'] = [
      '#type' => 'textfield',
      '#title' => t('Title login and password page'),
      '#required' => TRUE,
      '#default_value' => $this->config('trinion_base.settings')->get('title_login_and_password_page') ?? 'Trinion',
    ];
    $form['title_main_menu'] = [
      '#type' => 'textfield',
      '#title' => t('Title in top left menu'),
      '#required' => TRUE,
      '#default_value' => $this->config('trinion_base.settings')->get('title_main_menu') ?? 'Trinion',
    ];
    $form['trinion_ws_url'] = [
      '#type' => 'textfield',
      '#title' => 'URL вебсокет-сервера',
      '#default_value' => $this->config('trinion_base.settings')->get('trinion_ws_url'),
      '#description' => t('Example: wss://trinion.org/ws')
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('trinion_base.settings')
      ->set('backend_theme_name', $form_state->getValue('backend_theme_name'))
      ->set('host', $form_state->getValue('host'))
      ->set('title_login_and_password_page', $form_state->getValue('title_login_and_password_page'))
      ->set('title_main_menu', $form_state->getValue('title_main_menu'))
      ->set('trinion_ws_url', $form_state->getValue('trinion_ws_url'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
