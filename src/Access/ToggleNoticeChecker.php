<?php

namespace Drupal\trinion_base\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\Routing\Route;

/**
 * Check access to inline change field
 */
class ToggleNoticeChecker implements AccessInterface {

  /**
   * Access callback.
   */
  public function access(Route $route, Node $node) {
    if ($node->get('field_tb_polzovatel')->getString() == \Drupal::currentUser()->id())
      return AccessResult::allowed();
    else
      return AccessResult::forbidden();
  }
}
