<?php

namespace Drupal\trinion_base\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Symfony\Component\Routing\Route;

/**
 * Check access to inline change field
 */
class InlineChangeFieldChecker implements AccessInterface {

  /**
   * Access callback.
   */
  public function access(Route $route, $entity_type, $entity_id) {
    $user = User::load(\Drupal::currentUser()->id());
    $storage = \Drupal::entityTypeManager()->getStorage($entity_type);
    if ($storage instanceof SqlContentEntityStorage) {
      $entity = $storage->load($entity_id);
      return AccessResult::allowedIf($entity->access('view', $user));
    }
    else
      return AccessResult::forbidden();
  }
}
