<?php

namespace Drupal\trinion_base;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Twig extension.
 */
class TrinionBaseTwigExtension extends AbstractExtension {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new ZooModTwigExtension object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('getTerm', [$this, 'getTerm']),
      new TwigFunction('getNode', [$this, 'getNode']),
      new TwigFunction('getUser', [$this, 'getUser']),
      new TwigFunction('getQueryParam', [$this, 'getQueryParam']),
      new TwigFunction('getCurrentDestination', [$this, 'getCurrentDestination']),
      new TwigFunction('currentPageNid', [$this, 'currentPageNid']),
      new TwigFunction('getSetting', [$this, 'getSetting']),
      new TwigFunction('getPluralDate', [$this, 'getPluralDate']),
      new TwigFunction('getPluralCost', [$this, 'getPluralCost']),
      new TwigFunction('isModuleEnabled', [$this, 'isModuleEnabled']),
      new TwigFunction('pregReplace', [$this, 'pregReplace']),
    ];
  }

  public function getCurrentDestination() {
    return \Drupal::request()->getRequestUri();
  }

  public function getQueryParam($param) {
    return \Drupal::request()->get($param);
  }

  public function getTerm($tid) {
    return $this->entityTypeManager->getStorage('taxonomy_term')->load($tid);
  }

  public function getSetting($config_name, $param_name) {
    return \Drupal::config($config_name)->get($param_name);
  }

  public function getNode($nid) {
    return $this->entityTypeManager->getStorage('node')->load($nid);
  }

  public function getUser($uid) {
    return $this->entityTypeManager->getStorage('user')->load($uid);
  }

  public function getPluralDate($data) {
    return \Drupal::service('trinion_tp.helper')->getPluralDate($data);
  }

  public function currentPageNid() {
    return ($node = \Drupal::routeMatch()->getParameter('node')) ? $node->id() : '';
  }

  public function getPluralCost($value) {
    return \Drupal::service('trinion_main.helper')->getPluralCost($value);
  }

  public function isModuleEnabled($module) {
    $moduleHandler = \Drupal::service('module_handler');
    return $moduleHandler->moduleExists($module);
  }

  public function pregReplace($regexp, $replace, $string) {
    return preg_replace($regexp, $replace, $string);
  }
}
