<?php

namespace Drupal\trinion_base;

use Drupal\Component\Plugin\PluginInspectionInterface;

interface WidgetPluginInterface extends PluginInspectionInterface {

  public function getId();
  public function getData(array $options);
  public function getType();
  public function getTitle();
  public function getWeight();

}

