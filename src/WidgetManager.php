<?php
namespace Drupal\trinion_base;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

class WidgetManager extends DefaultPluginManager
{

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler)
  {
    parent::__construct(
      'Plugin/TrinionWidget',
      $namespaces,
      $module_handler,
      'Drupal\trinion_base\WidgetPluginInterface',
      'Drupal\trinion_base\Annotation\Widget'
    );
    $this->setCacheBackend($cache_backend, 'trinion_widget');
    $this->factory = new DefaultFactory($this->getDiscovery());
  }
}

