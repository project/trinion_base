<?php

namespace Drupal\trinion_base\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\Extension;
use Drupal\trinion_base\Widget;

class DashboardController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    $build['#attached']['library'][] = 'trinion_backend/charts';

    $plugin_service = \Drupal::service('plugin.manager.trinion_widget');
    $plugins = $plugin_service->getDefinitions();
    if ($plugins) {
      foreach ($plugin_service->getDefinitions() as $plugin_id => $plugin) {
        $instance = $plugin_service->createInstance($plugin_id);
        $build['content'][$instance->getId()] = [
          '#theme' => 'trinion_widget_' . $instance->getType(),
          '#data' => $instance->getData([]),
          '#title' => $instance->getTitle(),
          '#weight' => $instance->getWeight(),
          '#id' => $instance->getId(),
        ];
      }
    }
    else {
      $build['content'] = [
        '#markup' => t('Widgets list is empty'),
      ];
    }
    return $build;
  }
}
