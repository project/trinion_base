<?php

namespace Drupal\trinion_base\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Controller\ControllerBase;

class NewNoticesSwitcherController extends ControllerBase {

  public function build($val) {
    $uid = \Drupal::currentUser()->id();
    $val = $val == 'true';
    \Drupal::service('user.data')->set('trinion_base', $uid, 'new-notices-switcher', $val);
    $response = new AjaxResponse();
    $list = [
      '#theme' => 'trinion_notices_list_dropdown',
      '#data' => \Drupal\trinion_base\Controller\NoticeController::getUserNotices($val),
    ];
    $response->addCommand(new HtmlCommand('#notices-list-dropdown', $list));
    return $response;
  }
}
