<?php

namespace Drupal\trinion_base\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\FocusFirstCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;

class InlineChangeFieldController extends ControllerBase {

  public function showField($entity_type, $entity_id, $field_name) {
    $resposne = new AjaxResponse();
    $storage = \Drupal::entityTypeManager()->getStorage($entity_type);
    if ($storage instanceof SqlContentEntityStorage) {
      $entity = $storage->load($entity_id);
      switch ($field_name) {
        case 'field_tz_ikonka':
          $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['vid' => 'prioritet_zadachi']);
          $options = [];
          foreach ($terms as $term)
            $options[$term->id()] = $term->label();
          $build = [
            '#type' => 'select',
            '#options' => $options,
            '#attributes' => ['class' => ['tomselected']],
            '#value' => $entity->get('field_tz_prioritet')->getString(),
          ];
          break;
        case 'field_tz_ikonka_1':
          $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['vid' => 'tz_statusy_zadach']);
          $options = [];
          foreach ($terms as $term)
            $options[$term->id()] = $term->label();
          $build = [
            '#type' => 'select',
            '#options' => $options,
            '#attributes' => ['class' => ['tomselected']],
            '#value' => $entity->get('field_tz_status_zadachi')->getString(),
          ];
          break;
        case 'field_tz_kategoriya_zadachi':
          $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['vid' => 'kategoriya_zadachi']);
          $options = ['' => '-'];
          foreach ($terms as $term) {
            if ($entity->get('field_tz_proekt')->getString() == $term->get('field_tz_proekt')->getString())
              $options[$term->id()] = $term->label();
          }
          $build = [
            '#type' => 'select',
            '#options' => $options,
            '#attributes' => ['class' => ['tomselected']],
            '#value' => $entity->get('field_tz_kategoriya_zadachi')->getString(),
          ];
          break;
        case 'field_tz_otvetstvennyy':
          $options = [];
          $resposible = [];
          foreach ($entity->get('field_tz_otvetstvennyy') as $item)
            $resposible[] = $item->entity->id();
          $query = \Drupal::entityQuery('user')
            ->condition('field_tz_proekt', $entity->get('field_tz_proekt')->getString())
            ->condition('roles', 't_zadachnik');
          $res = $query->accessCheck()->execute();
          foreach (User::loadMultiple($res) as $user)
            $options[$user->id()] = $user->get('field_tb_nick_name')->getString();
          $build = [
            '#type' => 'select',
            '#attributes' => ['multiple' => 'multiple'],
            '#options' => $options,
            '#value' => $resposible,
          ];
          break;
        default:
          $build = \Drupal::moduleHandler()->invokeAll('trinion_base_inline_show_fields', [$field_name, $entity]);
          if (empty($build)) {
            $type = $entity->get($field_name)->getFieldDefinition()->getType();
            switch ($type) {
              case 'string':
                $build = [
                  '#type' => 'textfield',
                  '#value' => $entity->get($field_name)->getString(),
                ];
                break;
            }
          }
      }

      $resposne->addCommand(new HtmlCommand("#{$field_name}-{$entity_id}", $build));
      $resposne->addCommand(new FocusFirstCommand("#{$field_name}-{$entity_id}"));
    }
    return $resposne;
  }

  public function changeField($entity_type, $entity_id, $field_name) {
    $resposne = new AjaxResponse();
    $storage = \Drupal::entityTypeManager()->getStorage($entity_type);
    if ($storage instanceof SqlContentEntityStorage) {
      $entity = $storage->load($entity_id);
      $new_value = \Drupal::request()->get('data');
      switch ($field_name) {
        case 'field_tz_ikonka':
          $entity->set('field_tz_prioritet', $new_value);
          $entity->save();
          break;
        case 'field_tz_ikonka_1':
          $entity->set('field_tz_status_zadachi', $new_value);
          $entity->save();
          break;
        case 'field_tz_kategoriya_zadachi':
          if ($new_value) {
            $entity->set('field_tz_kategoriya_zadachi', $new_value ? $new_value : NULL);
            $entity->save();
          }
          break;
        case 'field_tz_otvetstvennyy':
          switch ($entity->bundle()) {
            case 'zadacha':
              $target_field = 'field_tz_otvetstvennyy';
              break;
          }
          if ($target_field) {
            $entity->set($target_field, $new_value);
            $entity->save();
          }
          break;
        default:
          $build = \Drupal::moduleHandler()->invokeAll('trinion_base_inline_change_fields', [$field_name, $entity, $new_value]);
          if (empty($build)) {
            $type = $entity->get($field_name)->getFieldDefinition()->getType();
            switch ($type) {
              case 'string':
                $entity->set($field_name, $new_value);
                $entity->save();
                $build = [$new_value];
                break;
            }
          }
          $resposne->addCommand(new HtmlCommand("#{$field_name}-{$entity_id}", reset($build)));
      }
    }
    return $resposne;
  }

  public function getValue($entity_type, $entity_id, $field_name) {
    $resposne = new AjaxResponse();
    $storage = \Drupal::entityTypeManager()->getStorage($entity_type);
    if ($storage instanceof SqlContentEntityStorage) {
      $entity = $storage->load($entity_id);

      $new_value = \Drupal::request()->get('data');
      switch ($field_name) {
        case 'field_tz_ikonka':
          $term = Term::load($new_value);
          $build = $term->get('field_tz_ikonka')->getString();
          break;
        case 'field_tz_ikonka_1':
          $term = Term::load($new_value);
          $build = $term->get('field_tz_ikonka')->getString();
          break;
        case 'field_tz_kategoriya_zadachi':
          if ($new_value) {
            $term = Term::load($new_value);
            $build = $term->label();
          } else
            $build = '';
          break;
        case 'field_tz_otvetstvennyy':
          switch ($entity->bundle()) {
            case 'zadacha':
              $target_field = 'field_tz_otvetstvennyy';
              break;
          }
          if ($target_field) {
            $build = [];
            foreach ($new_value as $uid) {
              if ($user = User::load($uid)) {
                $build[] = $user->get('field_tb_nick_name')->getString();
              }
            }

            $build = implode(', ', $build);
          } else {
            if ($current = $entity->get($target_field)->first()) {
              $build = $current->entity->get('field_tb_nick_name')->getString();
            }
          }
          break;
        default:
          $build = \Drupal::moduleHandler()->invokeAll('trinion_base_inline_change_fields', [$field_name, $entity, $new_value]);
          if (empty($build)) {
            $type = $entity->get($field_name)->getFieldDefinition()->getType();
            switch ($type) {
              case 'string':
                $build = $new_value;
                break;
            }
          } else
            $build = reset($build);
      }
      $resposne->addCommand(new HtmlCommand("#{$field_name}-{$entity_id}", $build));
    }

    return $resposne;
  }

  public function autocompleteField($node, $field_name) {
    $matches = [];
    $term = \Drupal::request()->get('term');
    switch ($field_name) {
      case 'field_tb_nick_name':
        $query = \Drupal::entityQuery('user')
          ->condition('field_tb_nick_name', $term . '%', 'LIKE');
        $query->range(0, 10);
        $res = $query->accessCheck()->execute();
        foreach (User::loadMultiple($res) as $user) {
          $value = $user->get('field_tb_nick_name')->getString();
          $value .= ' (' . $user->id() . ')';
          $matches[] = [
            'label' => $value,
            'value' => $value,
          ];
        }
        break;
    }
    return new JsonResponse($matches);
  }
}
