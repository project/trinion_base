<?php

namespace Drupal\trinion_base\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Controller\ControllerBase;

class MyAllSwitcherController extends ControllerBase {

  public function build($list, $val) {
    $uid = \Drupal::currentUser()->id();
    \Drupal::service('user.data')->set('trinion_base', $uid, 'my-all-switcher-' . $list, $val == 'true');
    return new AjaxResponse();
  }
}
