<?php

namespace Drupal\trinion_base\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Controller\ControllerBase;

class ToggleWidgetController extends ControllerBase {

  public function toggle($widget_id, $val) {
    $plugin_service = \Drupal::service('plugin.manager.trinion_widget');
    $plugins = $plugin_service->getDefinitions();
    $instance = $plugin_service->createInstance('my_task');
    $build['content'][$instance->getId()] = [
      '#theme' => 'trinion_widget_' . $instance->getType(),
      '#data' => $instance->getData(['my' => $val]),
      '#title' => $instance->getTitle(),
      '#weight' => $instance->getWeight(),
      '#my' => $val,
      '#id' => $instance->getId(),
    ];
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#widget-' . $widget_id, $build));
    return $response;
  }
}
