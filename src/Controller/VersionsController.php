<?php

namespace Drupal\trinion_base\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\Extension;

class VersionsController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    $rows = [];
    $module_list = \Drupal::service('extension.list.module')->getList();
    /**
     * @var Extension $module
     */
    foreach ($module_list as $name => $module) {
      if (strpos($name, 'trinion_') === 0) {
        $rows[] = [
          $module->info['name'],
          $module->info['version'],
        ];
      }
    }
    $build['content'] = [
      '#type' => 'table',
      '#rows' => $rows,
      '#attributes' => ['class' => ['table mb-3 table-bordered cols-8']],
    ];

    return $build;
  }

}
