<?php

namespace Drupal\trinion_base\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Mails extends ControllerBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Task comment notice
   */
  public function uvedomlenieNoviyCommentKZadache(\Drupal\comment\Entity\Comment $entity) {
    $node = $entity->getCommentedEntity();
    $node_owner = $node->get('uid')->entity;
    if ($entity->get('field_tz_private')->getString()) {
      if ($node_owner->get('field_tz_sotrudnik')->getString()) {
        $emails = [
          $node_owner->get('mail')->getString(),
        ];
        $uids = [
          $node_owner->id(),
        ];
      }
      foreach ($node->get('field_tz_otvetstvennyy') as $item) {
        if ($item->entity->get('field_tz_sotrudnik')->getString()) {
          $emails[] = $item->entity->get('mail')->getString();
          $uids[] = $item->entity->id();
        }
      }
    }
    else {
      $emails = [
        $node_owner->get('mail')->getString(),
      ];
      $uids = [
        $node_owner->id(),
      ];
      foreach ($node->get('field_tz_otvetstvennyy') as $item) {
        $emails[] = $item->entity->get('mail')->getString();
        $uids[] = $item->entity->id();
      }
    }

    if ($comment_owner = $entity->get('uid')->entity) {
      $emails = array_diff($emails, [$comment_owner->get('mail')->getString()]);
      $uids = array_diff($uids, [$comment_owner->id()]);
    }
    $emails = array_unique($emails);

    if ($emails) {
      $emails = implode(',', $emails);
      $params = $this->zadachaMailParams($node);
      $params['comment'] = $entity->get('field_tz_kommentariy')->getValue()[0]['value'];
      $mailManager = \Drupal::service('plugin.manager.mail');
      $langcode = \Drupal::currentUser()->getPreferredLangcode();
      $mailManager->mail('trinion_base', 'noviy_comment_k_zadache', $emails, $langcode, $params, NULL, TRUE);
    }
    if ($uids) {
      $text = t('New comment in task') . ' <a href="/node/' . $node->id() . '">#' . $node->label() . '</a> ' . $node->get('field_tz_tema')->getString();
      foreach ($uids as $uid)
        NoticeController::uvedomlenie($uid, $text);
    }
  }

  /**
   * Task comment notice
   */
  public function uvedomlenieNoviyCommentKDocumentu(\Drupal\comment\Entity\Comment $entity) {
    if ($entity->get('uid')->entity)
      $comment_owner = $entity->get('uid')->entity->get('mail')->getString();
    $node = $entity->getCommentedEntity();
    $emails = [
      $node->get('uid')->entity->get('mail')->getString(),
    ];
    if ($node->hasField('field_tl_otvetstvennyy'))
      $otvetstvenniy_field_name = 'field_tl_otvetstvennyy';
    else
      $otvetstvenniy_field_name = 'field_tp_otvetstvennyy';
    foreach ($node->get($otvetstvenniy_field_name) as $item) {
      $emails[] = $item->entity->get('mail')->getString();
    }
    if (!empty($comment_owner))
      $emails = array_diff($emails, [$comment_owner]);
    $emails = array_unique($emails);
    if ($emails) {
      $emails = implode(',', $emails);
      $params = $this->documentMailParams($node);
      $params['comment'] = $entity->get('field_tz_kommentariy')->getValue()[0]['value'];
      $mailManager = \Drupal::service('plugin.manager.mail');
      $langcode = \Drupal::currentUser()->getPreferredLangcode();
      $mailManager->mail('trinion_base', 'noviy_comment_k_documentu', $emails, $langcode, $params, NULL, TRUE);
    }
  }

  /**
   * New task notice
   */
  public function uvedomlenieNovayaZadacha(\Drupal\node\Entity\Node $entity) {
    $emails = [];
    foreach ($entity->get('field_tz_otvetstvennyy') as $item) {
      $emails[] = $item->entity->get('mail')->getString();
    }
    if ($emails) {
      $emails = implode(',', $emails);
      $params = $this->zadachaMailParams($entity);
      $mailManager = \Drupal::service('plugin.manager.mail');
      $langcode = \Drupal::currentUser()->getPreferredLangcode();
      $mailManager->mail('trinion_base', 'novya_zadacha', $emails, $langcode, $params, NULL, TRUE);
    }
  }

  /**
   * Change task notice
   */
  public function uvedomlenieObIzmeneniiZadachi(\Drupal\node\Entity\Node $entity) {
    $task = $entity->get('field_tcl_object')->first()->entity;
    if ($task->bundle() == 'zadacha') {
      $emails = [
        $task->get('uid')->entity->get('mail')->getString(),
      ];
      $uids = [$task->get('uid')->getString()];
      foreach ($task->get('field_tz_otvetstvennyy') as $item) {
        $emails[] = $item->entity->get('mail')->getString();
        $uids[] = $item->entity->id();
      }
    }
    if ($emails) {
      foreach ($entity->get('field_tcl_izmeneniya') as $item) {
        $field = $item->entity;
        $diffs[] = [
          'field' => $field->get('field_tcl_pole')->first()->entity->label(),
          'old' => ($first = $field->get('field_tcl_staroe_znachenie')->first()) ? $first->getValue()['value'] : '-',
          'new' => ($first = $field->get('field_tcl_novoe_znachenie')->first()) ? $first->getValue()['value'] : '-',
        ];
      }
      $params = $this->zadachaMailParams($task);
      $changes = [
        '#theme' => 'table',
        '#attributes' => ['style'=>['width:100%'], 'border' => '1', 'cellpadding' => '3', 'cellspacing' => '0'],
        '#header' => [t('Field'), t('Old value'), t('New value'),],
        '#rows' => $diffs,
      ];
      $params['text'] = (string)\Drupal::service('renderer')->render($changes);

      $emails = implode(',', $emails);
      $mailManager = \Drupal::service('plugin.manager.mail');
      $langcode = \Drupal::currentUser()->getPreferredLangcode();
      $mailManager->mail('trinion_base', 'izmeneniye_zadacha', $emails, $langcode, $params, NULL, TRUE);
    }
    if ($uids) {
      $uids = array_diff($uids, [\Drupal::currentUser()->id()]);
      $text = t('Task changed') . ' <a href="/node/' . $task->id() . '">#' . $task->label() . '</a> ' . $task->get('field_tz_tema')->getString();
      foreach ($uids as $uid)
        NoticeController::uvedomlenie($uid, $text);
    }
  }

  public function uvedomleniePolzovateliaORegistraciiPosleZakaza(Node $entity, $login, $password) {
    $params['login'] = $login;
    $params['password'] = $password;
    $mailManager = \Drupal::service('plugin.manager.mail');
    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $mailManager->mail('trinion_base', 'registratia_polzovatelia_zakaz', $entity->get('field_tp_email')->getString(), $langcode, $params, NULL, TRUE);
  }

  public function uvedomleniePolzovateliaObOplateKursa(Node $entity, Node $tovar) {
    $params['course_url'] = ($_SERVER['HTTP_ORIGIN'] ?? '') . Url::fromRoute('entity.node.canonical', ['node' => $tovar->id()])->toString();
    $params['course_name'] = $tovar->label();
    $mailManager = \Drupal::service('plugin.manager.mail');
    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $mailManager->mail('trinion_base', 'oplata_kursa', $entity->get('field_tp_email')->getString(), $langcode, $params, NULL, TRUE);
  }

  private function zadachaMailParams($entity) {
    $tb_helper = \Drupal::service('trinion_main.helper');
    $z_helper = \Drupal::service('trinion_zadachnik.helper');
    $host = $tb_helper->getCurrentHostName();
    if ($change_maker = User::load(\Drupal::currentUser()->id())) {
      $change_maker = \Drupal::service('trinion_main.helper')->getNameOrLogin($change_maker);
    }
    else
      $change_maker = '-';
    $params['nomer'] = $entity->label();
    $params['tema'] = $entity->get('field_tz_tema')->getString();
    $params['url'] = $host . Url::fromRoute('entity.node.canonical', ['node' => $entity->id()])->toString();
    $params['host'] = $host;
    $params['avtor'] = $tb_helper->getAvtorFromEntity($entity);
    $params['change_maker'] = $change_maker;
    $params['activnost'] = $z_helper->getZadachaDeyatelnostName($entity);
    $params['project'] = $z_helper->getZadachaProjectName($entity);
    $params['status'] = $z_helper->getZadachaStatusName($entity);
    $params['prioritet'] = $z_helper->getZadachaProirityName($entity);
    $params['otvetstvenniy'] = $z_helper->getZadachaReponsibleName($entity);
    if ($start_time = $entity->get('field_tz_vremya_nachala_zadachi')->getString())
      $params['start_time'] = \Drupal::service('date.formatter')->format(strtotime($start_time), 'custom', 'Y-m-d');
    $params['text'] = ($first = $entity->get('field_tz_opisanie')->first()) ? $first->getValue()['value'] : '';
    return $params;
  }

  private function documentMailParams($entity) {
    $tb_helper = \Drupal::service('trinion_main.helper');
    $host = $tb_helper->getCurrentHostName();
    $params['nomer'] = $entity->label();
    $params['url'] = $host . Url::fromRoute('entity.node.canonical', ['node' => $entity->id()])->toString();
    $params['host'] = $host;
    $params['avtor'] = $tb_helper->getAvtorFromEntity($entity);
    $params['tip_documenta'] = t(NodeType::load($entity->bundle())->label());
    return $params;
  }
}
