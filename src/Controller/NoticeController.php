<?php

namespace Drupal\trinion_base\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

class NoticeController extends ControllerBase {

  public function toggle(Node $node) {
    $response = new AjaxResponse();
    $node_id = $node->id();
    if ($node->get('field_tb_new_notice')->getString()) {
      $new_state = 0;
      $response->addCommand(new InvokeCommand("#notice-{$node_id} .status-dot", 'removeClass', ['status-dot-animated']));
      $response->addCommand(new InvokeCommand("#notice-{$node_id} .status-dot", 'removeClass', ['bg-red']));
      $response->addCommand(new InvokeCommand("#notice-{$node_id} .icon-tabler-check", 'hide'));
      $response->addCommand(new InvokeCommand("#notice-{$node_id} .icon-tabler-arrow-capsule", 'show'));
    }
    else {
      $new_state = 1;
      $response->addCommand(new InvokeCommand("#notice-{$node_id} .status-dot", 'addClass', ['status-dot-animated bg-red']));
      $response->addCommand(new InvokeCommand("#notice-{$node_id} .icon-tabler-check", 'show'));
      $response->addCommand(new InvokeCommand("#notice-{$node_id} .icon-tabler-arrow-capsule", 'hide'));
    }

    $node->field_tb_new_notice = $new_state;
    $node->save();
    $response->addCommand(new InvokeCommand('.has-new-notice', self::getCountNewUserNotices() ? 'show' : 'hide'));

    return $response;
  }

  public static function uvedomlenie($uid, $text) {
    $node = Node::create([
      'type' => 'trinion_uvedomlenie',
      'title' => \Drupal::service('trinion_main.helper')->getNextDocumentNumber('trinion_uvedomlenie'),
      'uid' => \Drupal::currentUser()->id(),
      'status' => 1,
      'field_tb_notice_text' => $text,
      'field_tb_polzovatel' => $uid,
    ]);
    $node->save();
  }

  public static function getUserNotices($new = NULL) {
    $query = \Drupal::entityQuery('node');
    $query->condition('field_tb_polzovatel', \Drupal::currentUser()->id());
    if ($new)
      $query->condition('field_tb_new_notice', 1);
    $query->sort('created', 'DESC');
    $query->range(0, 15);
    $res = $query->accessCheck()->execute();
    return $res ? Node::loadMultiple($res) : [];
  }

  public static function getCountNewUserNotices() {
    $query = \Drupal::entityQuery('node');
    $query->condition('field_tb_polzovatel', \Drupal::currentUser()->id());
    $query->condition('field_tb_new_notice', 1);
    $res = $query->accessCheck()->execute();
    return count($res);
  }
}
