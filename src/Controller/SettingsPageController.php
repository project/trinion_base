<?php

declare(strict_types=1);

namespace Drupal\trinion_base\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Trinion base routes.
 */
final class SettingsPageController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function __invoke(): array {
    $link_tree_service = \Drupal::service('menu.link_tree');
    $build['content'] = \Drupal::service('twig_tweak.menu_view_builder')->build('tb-settings', 1, 0, TRUE);

    return $build;
  }

}
