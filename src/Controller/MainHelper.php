<?php

namespace Drupal\trinion_base\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MainHelper extends ControllerBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  public function getDocumentOtvetstvenniy($node) {
    if ($node->hasField('field_tl_otvetstvennyy'))
      return $node->get('field_tl_otvetstvennyy')->getString();
    elseif ($node->hasField('field_tp_otvetstvennyy'))
      return $node->get('field_tp_otvetstvennyy')->getString();
    elseif ($node->hasField('field_tt_otvetstvennyy'))
      return $node->get('field_tt_otvetstvennyy')->getString();
  }

  public function getFormsBezItogoList() {
    return ['otgruzka', 'poluchennyy_platezh', 'otpravlennyy_platezh', 'tovar', 'cena', 'lead', 'contact', 'kompanii', 'zvonok', 'sdelki'];
  }

  public function getEntitiesTypeIsVocabularyList() {
    return ['kompanii', 'contact', 'tovar', 'lead', ];
  }

  public function getPluralCost($value) {
    $intPart = sprintf('%d', $value);
    $fractionalPart = round(($value - $intPart) * 100);

    $num = (new \NumberFormatter('ru-RU', \NumberFormatter::SPELLOUT))->format($intPart);
    $num .= ' ' . t('rub.') . ' ';
    $num .= (new \NumberFormatter('ru-RU', \NumberFormatter::SPELLOUT))->format($fractionalPart);
    $num .= ' ' . t('kop.');
    return $num;
  }

  public function getCurrentHostName() {
      return $_SERVER['HTTP_ORIGIN'] ?? \Drupal::config('trinion_base.settings')->get('host');
  }

  public function getAvtorFromEntity($entity) {
    $name = '';
    if ($entity && $entity->hasField('uid')) {
      $avtor_entity = $entity->get('uid')->first()->entity;
      $name = $this->getNameOrLogin($avtor_entity);
    }
    return $name;
  }

  public function getNameOrLogin($user) {
    $avtor[] = $user->get('field_tb_nick_name')->getString();
    $avtor = implode(' ', $avtor);
    if (trim($avtor) == '')
      $name = $user->label();
    else
      $name = $avtor;
    return $name;
  }

  public function getMyAllSwitcherValue($list) {
    $uid = \Drupal::currentUser()->id();
    return \Drupal::service('user.data')->get('trinion_base', $uid, 'my-all-switcher-' . $list);
  }

  public function getNextDocumentNumber($type) {
    $start_nomer = 1;
    $query = \Drupal::database()->select('node_field_data', 'n')
      ->condition('n.type', $type);
    $query->addField('n', 'title');
    $query->addExpression('CAST (n.title AS UNSIGNED)', 't');
    $query->orderBy('t', 'DESC');
    $query->range(0, 1);
    $res = $query->execute()->fetchField();
    return !$res || $res < $start_nomer ? $start_nomer : $res + 1;
  }
}
