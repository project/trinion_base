<?php

namespace Drupal\trinion_base\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

/**
 * Returns responses view document route.
 */
class ViewDocumentController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build($node) {
    $entity_type = 'node';
    $view_mode = 'full';
    $view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity_type);
    $build = $view_builder->view($node, $view_mode);

    $resposne = new AjaxResponse();
    $resposne->addCommand(new HtmlCommand('#document-content', $build));
    $resposne->addCommand(new InvokeCommand('.document-list .views-row', 'removeClass', ['active']));
    $resposne->addCommand(new InvokeCommand('.document-list .views-row[data-nid="' . $node->id() . '"]', 'addClass', ['active']));

    return $resposne;
  }

}
