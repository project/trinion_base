<?php

declare(strict_types=1);

namespace Drupal\trinion_base;

use Drupal\node\Entity\Node;

final class RelatedDocs {

  public function getPrevRelatedDocs(Node $node, $docs = []) {
    switch ($node->bundle()) {
      case 'mrp_zakaz_na_proizvodstvo':
        foreach ($node->get('field_mrp_report') as $item)
          $docs[] = $item->entity->id();
        break;
      case 'kommercheskoe_predlozhenie':
        foreach ($node->get('field_tp_sdelka') as $item)
          $docs[] = $item->entity->id();
        break;
      case 'zakaz_klienta':
        foreach ($node->get('field_tp_komm_predlozhenie') as $item) {
          $docs[] = $item->entity->id();
          $docs = $this->getPrevRelatedDocs($item->entity, $docs);
        }
        break;
      case 'schet':
      case 'zakaz_postavschiku':
      case 'otgruzka':
        foreach ($node->get('field_tp_zakaz_klienta') as $item) {
          $docs[] = $item->entity->id();
          $docs = $this->getPrevRelatedDocs($item->entity, $docs);
        }
        break;
      case 'schet_postavschika':
        foreach ($node->get('field_tp_zakaz_postavschika') as $item) {
          $docs[] = $item->entity->id();
          $docs = $this->getPrevRelatedDocs($item->entity, $docs);

          $query = \Drupal::entityQuery('node')
            ->condition('type', 'postuplenie_tovarov');
          $query->condition('field_tp_zakazy_postavschiku', $item->entity->id());
          $res = $query->accessCheck()->execute();
          foreach ($res as $id) {
            $docs[] = $id;
          }
        }
        break;
      case 'postuplenie_tovarov':
        foreach ($node->get('field_tp_zakazy_postavschiku') as $item) {
          $docs[] = $item->entity->id();
          $docs = $this->getPrevRelatedDocs($item->entity, $docs);

          $query = \Drupal::entityQuery('node')
            ->condition('type', 'schet_postavschika');
          $query->condition('field_tp_zakaz_postavschika', $item->entity->id());
          $res = $query->accessCheck()->execute();
          foreach ($res as $id) {
            $docs[] = $id;
          }
        }
        break;
      case 'poluchennyy_platezh':
      case 'otpravlennyy_platezh':
        foreach ($node->get('field_tp_stroki') as $item) {
          if ($invoice = $item->entity->get('field_tp_schet')->first()) {
            if ($invoice = $invoice->entity) {
              if ($id = $item->entity->get('field_tp_schet')->getString())
                $docs[] = $id;
              $docs = $this->getPrevRelatedDocs($invoice, $docs);
            }
          }
        }
        break;
    }
    return $docs;
  }

  public function getNextRelatedDocs(Node $node, $docs = [], $context = []) {
    $bundle = $node->bundle();
    switch ($bundle) {
      case 'sdelki':
        $query = \Drupal::entityQuery('node')
          ->condition('type', 'kommercheskoe_predlozhenie')
          ->condition('field_tp_sdelka', $node->id());
        $res = $query->accessCheck()->execute();
        foreach ($res as $id) {
          $docs[] = $id;
          $docs = $this->getNextRelatedDocs(Node::load($id), $docs);
        }
        break;
      case 'kommercheskoe_predlozhenie':
        $query = \Drupal::entityQuery('node')
          ->condition('type', 'zakaz_klienta')
          ->condition('field_tp_komm_predlozhenie', $node->id());
        $res = $query->accessCheck()->execute();
        foreach ($res as $id) {
          $docs[] = $id;
          $docs = $this->getNextRelatedDocs(Node::load($id), $docs);
        }
        break;
      case 'zakaz_klienta':
        $types = ['zakaz_postavschiku', 'schet'];
        if (!empty($context['from_type']))
          $types = array_diff($types, [$context['from_type']]);
        unset($context['from_type']);
        $query = \Drupal::entityQuery('node')
          ->condition('type', $types, 'IN')
          ->condition('field_tp_zakaz_klienta', $node->id());
        $res = $query->accessCheck()->execute();
        foreach ($res as $id) {
          $docs[] = $id;
          $docs = $this->getNextRelatedDocs(Node::load($id), $docs, $context);
        }
        break;
      case 'otgruzka':
        if (empty($context['skip_up_tree']))
          foreach ($node->get('field_tp_zakaz_klienta') as $item)
            $docs = $this->getNextRelatedDocs(Node::load($item->entity->id()), $docs, ['from_type' => $bundle, 'skip_up_tree' => TRUE]);
        break;
      case 'schet':
        if (empty($context['skip_up_tree']))
          foreach ($node->get('field_tp_zakaz_klienta') as $item)
            $docs = $this->getNextRelatedDocs(Node::load($item->entity->id()), $docs, ['from_type' => $bundle, 'skip_up_tree' => TRUE]);

        $query = \Drupal::entityQuery('node')
          ->condition('type', 'tp_stroka_schet_klienta_k_oplate')
          ->condition('field_tp_schet', $node->id());
        $res = $query->accessCheck()->execute();
        foreach ($res as $id) {
          if ($row_node = Node::load($id)) {
            if ($nid = $row_node->get('field_tp_roditelskiy_dokument')->getString())
              $docs[] = $nid;
          }
        }
        break;
      case 'zakaz_postavschiku':
        $types = ['schet_postavschika', 'postuplenie_tovarov'];
        if (!empty($context['from_type']))
          $types = array_diff($types, [$context['from_type']]);
        unset($context['from_type']);
        $query = \Drupal::entityQuery('node')
          ->condition('type', $types, 'IN');
        $or = $query->orConditionGroup();
        $or->condition('field_tp_zakaz_postavschika', $node->id());
        $or->condition('field_tp_zakazy_postavschiku', $node->id());
        $query->condition($or);
        $res = $query->accessCheck()->execute();
        foreach ($res as $id) {
          if (!in_array($id, $docs)) {
            $docs[] = $id;
            $docs = $this->getNextRelatedDocs(Node::load($id), $docs, $context);
          }
        }

        if (empty($context['skip_up_tree']))
          foreach ($node->get('field_tp_zakaz_klienta') as $item)
            $docs = $this->getNextRelatedDocs(Node::load($item->entity->id()), $docs, ['from_type' => 'zakaz_postavschiku', 'skip_up_tree' => TRUE]);
        break;
      case 'schet_postavschika':
        foreach ($node->get('field_tp_zakaz_postavschika') as $item) {
          if ($item = $item->entity) {
            $docs = $this->getNextRelatedDocs($item, $docs, ['from_type' => 'schet_postavschika']);
          }
        }
        $query = \Drupal::entityQuery('node')
          ->condition('type', 'stroka_scheta_prodavca_k_oplate')
          ->condition('field_tp_schet', $node->id());
        $res = $query->accessCheck()->execute();
        foreach ($res as $id) {
          if ($row_node = Node::load($id)) {
            if ($nid = $row_node->get('field_tp_roditelskiy_dokument')->getString())
              $docs[] = $nid;
          }
        }
        break;
      case 'postuplenie_tovarov':
        foreach ($node->get('field_tp_zakazy_postavschiku') as $item) {
          if ($item = $item->entity) {
            $docs = $this->getNextRelatedDocs($item, $docs, ['from_type' => 'postuplenie_tovarov']);
          }
        }
        break;
      case 'poluchennyy_platezh':
        foreach ($node->get('field_tp_stroki') as $item) {
          if ($invoice = $item->entity->get('field_tp_schet')->first()) {
            if ($invoice = $invoice->entity) {
              $docs = $this->getNextRelatedDocs($invoice, $docs);
            }
          }
        }
        $moduleHandler = \Drupal::service('module_handler');
        if ($moduleHandler->moduleExists('trinion_client_bank')) {
          $query = \Drupal::entityQuery('node')
            ->condition('type', 'trinion_payment_client_bank')
            ->condition('field_tcb_platezh', $node->id());
          $res = $query->accessCheck()->execute();
          if ($res)
            $docs[] = reset($res);
        }
        break;
      case 'otpravlennyy_platezh':
        foreach ($node->get('field_tp_stroki') as $item) {
          if ($bill = $item->entity->get('field_tp_schet')->first()) {
            if ($bill = $bill->entity) {
              $docs = $this->getNextRelatedDocs($bill, $docs);
            }
          }
        }
        $moduleHandler = \Drupal::service('module_handler');
        if ($moduleHandler->moduleExists('trinion_client_bank')) {
          $query = \Drupal::entityQuery('node')
            ->condition('type', 'trinion_payment_client_bank')
            ->condition('field_tcb_platezh', $node->id());
          $res = $query->accessCheck()->execute();
          if ($res)
            $docs[] = reset($res);
        }
        break;
      case 'trinion_payment_client_bank':
        if ($platezh_id = $node->get('field_tcb_platezh')->getString())
          $docs[] = $platezh_id;
        break;
    }
    return $docs;
  }

  public function getRalatedDocs(Node $node) {
    $docs = $this->getPrevRelatedDocs($node);
    $docs[] = $node->id();
    $docs = $this->getNextRelatedDocs($node, $docs);
    return $docs;
  }

}
