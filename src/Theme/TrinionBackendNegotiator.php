<?php

namespace Drupal\trinion_base\Theme;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\user\Entity\User;

/**
 * Defining theme for crm
 */
class TrinionBackendNegotiator implements ThemeNegotiatorInterface {

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $has_trinion_tp = \Drupal::hasService('trinion_tp.helper');
    $route_name = (string)$route_match->getRouteName();
    if ($route_name == 'entity.user.edit_form') {
      $destination = \Drupal::request()->get('destination');
      if ($destination && strpos($destination, '/admin/trinion_users') === 0) {
        return TRUE;
      }
    }
    if (in_array($route_name, ['user.login',  'user.pass', 'user.register', 'user.reset.form', 'user.logout.confirm', 'view.trinion_polzovateli.page_1']))
      return TRUE;
    if (strpos($route_name, 'trinion_permissions.') === 0)
      return TRUE;
    if ($route_name == 'trinion_zadachnik.task_by_project_access')
      return TRUE;

    if ($route_name == 'entity.taxonomy_term.canonical') {
      if (in_array($route_match->getParameter('taxonomy_term')->bundle(), ['organizaciya']))
        return TRUE;
    }
    elseif (strpos($route_name, 'trinion_tp.pdf.') === 0) {
      return TRUE;
    }
    elseif (strpos($route_name, 'trinion_crm.pdf.') === 0) {
      return TRUE;
    }
    elseif ($route_name == 'entity.node.edit_form' || $route_name == 'entity.node.delete_form' || $route_name == 'entity.node.canonical') {
      $bundle = $route_match->getParameter('node')->bundle();
      if (in_array($bundle, ['sales_order', 'lead', 'contact', 'sdelki', 'zvonok', 'kompanii', 'kommercheskoe_predlozhenie', 'cena', 'zakaz_klienta', 'schet', 'zakaz_postavschiku', 'poluchennyy_platezh', 'otpravlennyy_platezh', 'zakaz_postavschiku', 'schet_postavschika', 'otgruzka', 'postuplenie_tovarov',  'ostatok', 'ostatki_materilov_v_proizvodstve', 'rezerv', 'kupon',  'zadacha', 'change_log',  'stroka_istorii_izmeneniy', 'mail', 'peremeshchenie_tovarov', 'trebovanie_nakladnaya', 'vypusk_produkcii', 'zapis_mrp',  'mps', 'mrp_specifikaciya', 'mrp_rabochiy_centr', 'mrp_rabochee_podrazdelenie', 'tp_sales_plan', 'mrp_zakaz_na_proizvodstvo', 'trinion_payment_client_bank', 'shablon_biznes_processa', 'bpmn_zadacha', 'trinion_bpmn_sobytie', 'bpmn_shlyuz', 'bpmn_potok', 'biznes_process', ]))
        return TRUE;
      if ($has_trinion_tp && in_array($bundle, \Drupal::service('trinion_tp.helper')->getProductBundles())) {
        if (\Drupal::currentUser()->hasPermission('trinion_tp tovari'))
          return TRUE;
      }
    }
    elseif ($route_name == 'entity.user.canonical') {
        return TRUE;
    }
    elseif ($route_name == 'node.add') {
      $bundle = $route_match->getParameter('node_type')->id();
      if ($has_trinion_tp && in_array($bundle, \Drupal::service('trinion_tp.helper')->getProductBundles()))
        return TRUE;
      switch ($bundle) {
        case 'contact':
        case 'lead':
        case 'sales_order':
        case 'sdelki':
        case 'zvonok':
        case 'kompanii':
        case 'kommercheskoe_predlozhenie':
        case 'cena':
        case 'zakaz_klienta':
        case 'schet':
        case 'poluchennyy_platezh':
        case 'otpravlennyy_platezh':
        case 'zakaz_postavschiku':
        case 'schet_postavschika':
        case 'otgruzka':
        case 'postuplenie_tovarov':
        case 'kupon':
        case 'zadacha':
        case 'peremeshchenie_tovarov':
        case 'trebovanie_nakladnaya':
        case 'vypusk_produkcii':
        case 'zapis_mrp':
        case 'shablon_biznes_processa':
        case 'mps':
        case 'mrp_specifikaciya':
        case 'mrp_rabochiy_centr':
        case 'mrp_rabochee_podrazdelenie':
        case 'mrp_zakaz_na_proizvodstvo':
        case 'trinion_payment_client_bank':
        case 'tp_sales_plan':
        case 'biznes_process':
          return TRUE;
          break;
      }
    }
    else {
      if (strpos($route_name, 'trinion_reports.') === 0)
        return TRUE;

      switch ($route_name) {
        case 'entity.user.edit_form':
        case 'trinion_srec.get_form_appointment_select_doctor':
        case 'trinion_srec.appointment':
        case 'trinion_crm.lead_convert':
        case 'trinion_tp.poluchenniy_platezh_sozdanie':
        case 'trinion_tp.otpravlenniy_platezh_sozdanie':
        case 'view.crm_spisok_kontakty.page_1':
        case 'view.crm_spisok_poluchennye.page_1':
        case 'view.lidy.page_1':
        case 'view.lidy.page_2':
        case 'view.crm_spisok_so.page_1':
        case 'view.crm_spisok_sdelki.page_1':
        case 'view.crm_spisok_zvonki.page_1':
        case 'view.crm_spisok_zvonki.page_2':
        case 'view.crm_spisok_kompanii.page_1':
        case 'view.crm_spisok_kommercheskikh_predlozheniy.page_1':
        case 'view.crm_spisok_tovary.page_1':
        case 'view.crm_spisok_zakazy_klienta.page_1':
        case 'view.crm_spisok_scheta_klienta.page_1':
        case 'view.crm_spisok_zakazy_postavschiku.page_1':
        case 'view.crm_spisok_schetov_postavschika.page_1':
        case 'view.crm_spisok_sdelki.page_1':
        case 'view.crm_list_of_client_bank_payments.page_1':
        case 'view.tp_sales_plans.page_1':
        case 'view.crm_spisok_otgruzki.page_1':
        case 'view.crm_list_of_mrp_specifikacii.page_1':
        case 'view.crm_list_of_mps.page_1':
        case 'view.crm_list_of_worp_centers.page_1':
        case 'view.crm_list_of_work_devisions.page_1':
        case 'view.crm_list_of_production_orders.page_1':
        case 'view.crm_spisok_otpravlennykh_platezhey.page_1':
        case 'view.crm_spisok_postupleniya_tovarov.page_1':
        case 'view.crm_list_of_movement_goods.page_1':
        case 'view.crm_list_of_trebovanie_nakladnaya.page_1':
        case 'view.crm_list_of_vipusk_produkcii.page_1':
        case 'view.crm_spisok_ostatkov.page_1':
        case 'view.crm_list_of_balances_in_production.page_1':
        case 'view.crm_spisok_rezervov.page_1':
        case 'view.crm_spisok_kupony.page_1':
        case 'view.crm_list_of_mrp_reports.page_1':
        case 'view.zadachi_naznachennye_mne.page_1':
        case 'view.zadachi_naznachennye_mne.page_4':
        case 'view.crm_spisok_pochta.page_1':
        case 'view.istoriya_izmeneniy.page_1':
        case 'view.istoriya_izmeneniy.page_2':
        case 'view.trinion_task_categories.page_1':
        case 'view.trinion_terms.page_1':
        case 'view.tbi_dostupnye_biznes_processy.page_1':
        case 'view.tbi_biznes_processy.page_1':
        case 'entity.taxonomy_term.add_form':
        case 'entity.taxonomy_term.edit_form':
        case 'trinion_tp.frontol_vigruzka_tovarov':
        case 'trinion_tp.frontol_vigruzka_tovarov_save':
        case 'trinion_tp.frontol_vigruzka_zakazov':
        case 'trinion_tel.test_popup':
        case 'trinion_base.versions':
        case 'trinion_base.dashboard':
        case 'trinion_tp.sozdanie_tovara':
        case 'trinion_tp.sozdanie_tovara':
        case 'views_bulk_edit.edit_form':
        case 'trinion_change_log.status_zadach_otchet':
        case 'trinion_mrp.bom_report':
        case 'trinion_mrp.mrp_record_creator':
        case 'trinion_mrp.proizvodstvenniy_kalendar':
        case 'trinion_base.settings_page':
        case 'trinion_client_bank.client_bank_import_payments':
        case 'trinion_live_chat.operator_chat_page':
          return TRUE;
          break;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    $theme_name = \Drupal::config('trinion_base.settings')->get('backend_theme_name');
    if (is_null($theme_name))
      $theme_name = 'trinion_backend';
    return $theme_name;
  }
}
