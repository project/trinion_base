<?php

namespace Drupal\trinion_base;

use Drupal\Component\Plugin\PluginBase;

abstract class WidgetPluginBase extends PluginBase implements WidgetPluginInterface {

  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  public function getId() {
    return $this->pluginDefinition['id'];
  }

  public function getTitle() {
    return $this->pluginDefinition['title'];
  }

  public function getType() {
    return $this->pluginDefinition['type'];
  }

  public function getWeight() {
    return 1;
  }

  public function getData(array $options) {
    return [];
  }

}

