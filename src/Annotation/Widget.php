<?php

namespace Drupal\trinion_base\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Annotation for plugin Widget
 *
 * @Annotation
 */
class Widget extends Plugin {

  /**
   * plugin ID.
   */
  public $id;

  /**
   * Title.
   */
  public $title;

  /**
   * Chart type.
   * Allowed values: pie
   */
  public $type;

}

