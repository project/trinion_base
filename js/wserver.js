(function ($, Drupal) {
  Drupal.behaviors.wserver = {
    attach: function (context, settings) {
      once('ws', 'body', context).forEach(function (element) {
        var modal_new_call, call_info_modal;
        function connect() {
          if (settings.ws_url == null || settings.ws_url == '')
            return;
          var client = new WebSocket(settings.ws_url);
          client.binaryType = 'blob';

          client.onerror = function (e) {

            console.log(e);
            console.log('Connection Error');
          };

          client.onopen = function () {
            console.log('WebSocket Client Connected');
            data = {
              action: 'setUid',
              data: settings.currentUserUid
            };
            client.send(JSON.stringify(data));
          };

          client.onclose = function () {
            console.log('echo-protocol Client Closed');
            setTimeout(function() {
              connect();
            }, 1000);
          };

          client.onmessage = function (e) {
            console.log(e);
            if (typeof e.data === 'string') {
              console.log("Received: '" + e.data + "'");
            }
            data = JSON.parse(e.data);
            if (data.action == 'newCallPopup') {
              newPickupPopup(data.data);
            }

            if (data.action == 'newPickupPopup') {
              newPickupPopup(data.data);
            }

            if (data.action == 'callFinish') {
              $("#call-modal .save").show();
            }

            if (data.action == 'newChatMessage') {
              var msg_data = {msgs: [data.data]};
              Drupal.behaviors.chat.appendMessagesClient(msg_data);
              var msg = msg_data.msgs[0];
              // if (data.data.who == 'o') {
              //   var audio = new Audio('/modules/contrib/trinion_live_chat/sound/income-msg.wav');
              //   audio.play();
              // }
              if ($("#chats-list").length > 0) {
                if ($("#chats-list li[data-chat_id='" + msg.chat_id + "']").length == 0) {
                  Drupal.behaviors.chat.refreshChatsList();
                }
                else
                  $("#chats-list li[data-chat_id='" + msg.chat_id + "'] p").html(msg.msg);
              }
            }
          };
        }
        connect();

        $("#test-open-new-pickup-popup").on('click', function() {
          var data = {
            phoneNumber: '+79000000000',
            clientName: 'John Smith',
            companyName: 'OOO Крылатые качели',
            companyId: 2,
            callId: '0'
          };
          newPickupPopup(data);
        });

        $(".start-call-link").on('click', function() {
          var url = '/rest/telephony.start_call/' + $(this).data("id");
          $.ajax(url, {
            success: function(data) {
              if (data.hasOwnProperty('errors')) {
                Swal.fire('Ошибка', data.errors.join('<br>'), 'error');
              }
            }
          });
        });

        function newPickupPopup(data) {
          var text = data.clientName ? data.clientName : data.phoneNumber;
          if (data.companyName)
            text = text + '<br>' + data.companyName;
          $('#call-modal .text').html(text);
          $('#call-modal svg.icon').hide();
          if (data.type == 'incoming') {
            $('#call-modal svg.icon-tabler-phone-incoming').show();
          }
          else {
            $('#call-modal svg.icon-tabler-phone-outgoing').show();
          }
          if (modal_new_call)
            modal_new_call.hide();
          call_info_modal = new bootstrap.Modal(document.getElementById('call-modal'), {
            keyboard: false
          })
          call_info_modal.show();
          $('#call-modal .add-contact').click(function(){
            var url = `/node/add/contact?phone=${data.phoneNumber}&company=${data.companyId}`;
            window.open(url, '_blank').focus();
          });
          $('#call-modal .save').click(function(){
            var url = '/rest/telephony.externalcall.savedescription';
            $.ajax(url, {
              data: {
                call_id: data.callId,
                text: $("#call-modal textarea").val(),
              },
              dataType: 'json',
              method: "POST",
              success: function(data) {
                call_info_modal.hide();
                if (data.hasOwnProperty('errors')) {
                  console.log(data);
                }
              }
            });
          });
        }
      });
    }
  };
})(jQuery, Drupal);
